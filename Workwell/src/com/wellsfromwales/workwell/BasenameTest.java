package com.wellsfromwales.workwell;

public class BasenameTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String regex1 = "/(?=[^/]+$)";
		String regex2 = "\\.(?=[^\\.]+$)";
		String theString = "regex/awesome.txt";
		if (theString.matches("(.*/.+)+"))
			theString = theString.split(regex1)[1];
		if (theString.matches(".+\\..+"))
			theString = theString.split(regex2)[0];
		System.out.println(theString);
	}

}
