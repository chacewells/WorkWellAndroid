package com.wellsfromwales.workwell;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName=GuidedMeditation.TABLE_NAME)
public class GuidedMeditation {
	public static final String TABLE_NAME = "GuidedMeditations",
			ID_COLUMN = "_id",
			TITLE_COLUMN = "title",
			AUDIO_FILE_ID_COLUMN = "audiofile_id";
	
	@DatabaseField(generatedId=true, columnName=ID_COLUMN)
	public int id;
	@DatabaseField(canBeNull=false, columnName=TITLE_COLUMN)
	public String title;
	@DatabaseField(foreign=true, foreignAutoRefresh=true, canBeNull=false, columnName=AUDIO_FILE_ID_COLUMN)
	private AudioFile audioFile;
	
	public GuidedMeditation() {
//		DatabaseHelper needs a no arg constructor
	}
	
	public GuidedMeditation(String title, AudioFile audioFile) {
		this.title = title;
		this.audioFile = audioFile;
	}

	public AudioFile getAudioFile() {
		return audioFile;
	}

	public void setAudioFile(AudioFile audioFile) {
		this.audioFile = audioFile;
	}

	@Override
	public String toString() {
		return "GuidedMeditation [id=" + id + ", title=" + title
				+ ", audioFile=" + audioFile + "]";
	}

}
