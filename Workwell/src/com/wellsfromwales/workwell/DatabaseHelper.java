package com.wellsfromwales.workwell;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.wellsfromwales.workwell.R;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
	public static final String DATABASE_NAME = "WorkWell.db";
	private static final int DATABASE_VERSION = 1;
	private static final String TAG = "WorkWell";
	
	private Dao<Location, Integer> locationDao = null;
	private Dao<Course, Integer> courseDao = null;
	private Dao<User, Integer> userDao = null;
	private Dao<PracticeSession, Integer> practiceSessionDao = null;
	private Dao<AudioFile, Integer> audioFileDao = null;
	private Dao<GuidedMeditation, Integer> guidedMeditationDao = null;
	private Dao<MindfulMinuteInstance, Integer> mindfulMinuteInstanceDao = null;
	private Dao<MindfulMinuteTemplate, Integer> mindfulMinuteTemplateDao = null;
	
	private RuntimeExceptionDao<Location, Integer> locationRuntimeExceptionDao = null;
	private RuntimeExceptionDao<Course, Integer> courseRuntimeExceptionDao = null;
	private RuntimeExceptionDao<User, Integer> userRuntimeExceptionDao = null;
	private RuntimeExceptionDao<PracticeSession, Integer> practiceSessionRuntimeExceptionDao = null;
	private RuntimeExceptionDao<AudioFile, Integer> audioFileRuntimeExceptionDao = null;
	private RuntimeExceptionDao<GuidedMeditation, Integer> guidedMeditationRuntimeExceptionDao = null;
	private RuntimeExceptionDao<MindfulMinuteInstance, Integer> mindfulMinuteInstanceRuntimeExceptionDao = null;
	private RuntimeExceptionDao<MindfulMinuteTemplate, Integer> mindfulMinuteTemplateRuntimeExceptionDao = null;
	
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
	}

	@Override
	public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
		try {
			TableUtils.createTable(connectionSource, Location.class);
			TableUtils.createTable(connectionSource, Course.class);
			TableUtils.createTable(connectionSource, User.class);
			TableUtils.createTable(connectionSource, PracticeSession.class);
			TableUtils.createTable(connectionSource, AudioFile.class);
			TableUtils.createTable(connectionSource, GuidedMeditation.class);
			TableUtils.createTable(connectionSource, MindfulMinuteInstance.class);
			TableUtils.createTable(connectionSource, MindfulMinuteTemplate.class);
		} catch (SQLException e) {
			Log.e(TAG, "Unable to create tables.", e);
			
			throw new RuntimeException(e);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion,
			int newVersion) {
		try {
			TableUtils.dropTable(connectionSource, MindfulMinuteTemplate.class, true);
			TableUtils.dropTable(connectionSource, MindfulMinuteInstance.class, true);
			TableUtils.dropTable(connectionSource, GuidedMeditation.class, true);
			TableUtils.dropTable(connectionSource, AudioFile.class, true);
			TableUtils.dropTable(connectionSource, PracticeSession.class, true);
			TableUtils.dropTable(connectionSource, User.class, true);
			TableUtils.dropTable(connectionSource, Course.class, true);
			TableUtils.dropTable(connectionSource, Location.class, true);
			onCreate(database, connectionSource);
		} catch (SQLException e) {
			Log.e(TAG, "Unable to update database version.", e);
			
			throw new RuntimeException(e);
		}
	}
	
	public Dao<Location, Integer> getLocationDao() {
		return locationDao;
	}
	public Dao<Course, Integer> getCourseDao() throws SQLException {
		if (courseDao == null)
			courseDao = getDao(Course.class);
		return courseDao;
	}
	public Dao<User, Integer> getUserDao() throws SQLException {
		if (userDao == null)
			userDao = getDao(User.class);
		return userDao;
	}
	public Dao<PracticeSession, Integer> getPracticeSessionDao() throws SQLException {
		if (practiceSessionDao == null)
			practiceSessionDao = getDao(PracticeSession.class);
		return practiceSessionDao;
	}
	public Dao<AudioFile, Integer> getAudioFileDao() throws SQLException {
		if (audioFileDao == null)
			audioFileDao = getDao(AudioFile.class);
		return audioFileDao;
	}
	public Dao<GuidedMeditation, Integer> getGuidedMeditationDao() throws SQLException {
		if (guidedMeditationDao == null)
			guidedMeditationDao = getDao(GuidedMeditation.class);
		return guidedMeditationDao;
	}
	public Dao<MindfulMinuteInstance, Integer> getMindfulMinuteInstanceDao() throws SQLException {
		if (mindfulMinuteInstanceDao == null)
			mindfulMinuteInstanceDao = getDao(MindfulMinuteInstance.class);
		return mindfulMinuteInstanceDao;
	}
	public Dao<MindfulMinuteTemplate, Integer> getMindfulMinuteTemplateDao() throws SQLException {
		if (mindfulMinuteTemplateDao == null)
			mindfulMinuteTemplateDao = getDao(MindfulMinuteTemplate.class);
		return mindfulMinuteTemplateDao;
	}
	public RuntimeExceptionDao<Location, Integer> getLocationRuntimeExceptionDao() {
		if (locationRuntimeExceptionDao == null)
			locationRuntimeExceptionDao = getRuntimeExceptionDao(Location.class);
		return locationRuntimeExceptionDao;
	}
	public RuntimeExceptionDao<Course, Integer> getCourseRuntimeExceptionDao() {
		if (courseRuntimeExceptionDao == null)
			courseRuntimeExceptionDao = getRuntimeExceptionDao(Course.class);
		return courseRuntimeExceptionDao;
	}
	public RuntimeExceptionDao<User, Integer> getUserRuntimeExceptionDao() {
		if (userRuntimeExceptionDao == null)
			userRuntimeExceptionDao = getRuntimeExceptionDao(User.class);
		return userRuntimeExceptionDao;
	}
	public RuntimeExceptionDao<PracticeSession, Integer> getPracticeSessionRuntimeExceptionDao() {
		if (practiceSessionRuntimeExceptionDao == null)
			practiceSessionRuntimeExceptionDao = getRuntimeExceptionDao(PracticeSession.class);
		return practiceSessionRuntimeExceptionDao;
	}
	public RuntimeExceptionDao<AudioFile, Integer> getAudioFileRuntimeExceptionDao() {
		if (audioFileRuntimeExceptionDao == null)
			audioFileRuntimeExceptionDao = getRuntimeExceptionDao(AudioFile.class);
		return audioFileRuntimeExceptionDao;
	}
	public RuntimeExceptionDao<GuidedMeditation, Integer> getGuidedMeditationRuntimeExceptionDao() {
		if (guidedMeditationRuntimeExceptionDao == null)
			guidedMeditationRuntimeExceptionDao = getRuntimeExceptionDao(GuidedMeditation.class);
		return guidedMeditationRuntimeExceptionDao;
	}
	public RuntimeExceptionDao<MindfulMinuteInstance, Integer> getMindfulMinuteInstanceRuntimeExceptionDao() {
		if (mindfulMinuteInstanceRuntimeExceptionDao == null)
			mindfulMinuteInstanceRuntimeExceptionDao = getRuntimeExceptionDao(MindfulMinuteInstance.class);
		return mindfulMinuteInstanceRuntimeExceptionDao;
	}
	public RuntimeExceptionDao<MindfulMinuteTemplate, Integer> getMindfulMinuteTemplateRuntimeExceptionDao() {
		if (mindfulMinuteTemplateRuntimeExceptionDao == null)
			mindfulMinuteTemplateRuntimeExceptionDao = getRuntimeExceptionDao(MindfulMinuteTemplate.class);
		return mindfulMinuteTemplateRuntimeExceptionDao;
	}
	
}
