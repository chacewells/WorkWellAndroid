package com.wellsfromwales.workwell;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName=MindfulMinuteInstance.TABLE_NAME)
public class MindfulMinuteInstance {
	public static final String TABLE_NAME = "MindfulMinuteInstances",
			ID_COLUMN = "_id",
			TIME_OF_DAY_COLUMN = "timeofday";
	
	@DatabaseField(generatedId=true, columnName=ID_COLUMN)
	public int id;
	@DatabaseField(canBeNull=false, columnName=TIME_OF_DAY_COLUMN)
	public double timeOfDay;
	
	public MindfulMinuteInstance() {
//		DatabaseHelper needs a no arg constructor
	}

	public MindfulMinuteInstance(double timeOfDay) {
		super();
		this.timeOfDay = timeOfDay;
	}

	@Override
	public String toString() {
		return "MindfulMinuteInstance [id=" + id + ", timeOfDay=" + timeOfDay
				+ "]";
	}
	
}
