package com.wellsfromwales.workwell;

import java.io.IOException;
import java.sql.SQLException;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

public class DatabaseConfigUtil extends OrmLiteConfigUtil {
	private static Class<?>[] classes = new Class<?>[]{
			AudioFile.class,
			Course.class,
			GuidedMeditation.class,
			Location.class,
			MindfulMinuteInstance.class,
			MindfulMinuteTemplate.class,
			PracticeSession.class,
			User.class};
	
	public static void main(String[] args) throws SQLException, IOException {
		writeConfigFile("ormlite_config.txt", classes);
	}

}
