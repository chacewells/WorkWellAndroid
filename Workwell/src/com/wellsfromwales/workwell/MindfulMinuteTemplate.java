package com.wellsfromwales.workwell;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName=MindfulMinuteTemplate.TABLE_NAME)
public class MindfulMinuteTemplate {
	public static final String TABLE_NAME = "MindfulMinuteTemplates",
			ID_COLUMN = "_id",
			ALERT_BODY_COLUMN = "alertbody",
			SOUND_NAME_COLUMN = "soundname";
	
	@DatabaseField(generatedId=true, columnName=ID_COLUMN)
	public int id;
	@DatabaseField(canBeNull=false, columnName=ALERT_BODY_COLUMN)
	public String alertBody;
	@DatabaseField(canBeNull=false, columnName=SOUND_NAME_COLUMN)
	public String soundName;
	
	public MindfulMinuteTemplate() {
//		DatabaseHelper needs a no arg constructor
	}

	public MindfulMinuteTemplate(String alertBody, String soundName) {
		super();
		this.alertBody = alertBody;
		this.soundName = soundName;
	}

	@Override
	public String toString() {
		return "MindfulMinuteTemplate [id=" + id + ", alertBody=" + alertBody
				+ ", soundName=" + soundName + "]";
	}
}
