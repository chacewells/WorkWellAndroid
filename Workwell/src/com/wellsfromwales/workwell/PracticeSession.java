package com.wellsfromwales.workwell;

import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName=PracticeSession.TABLE_NAME)
public class PracticeSession {
	public static final String TABLE_NAME = "PracticeSessions",
			ID_COLUMN = "_id",
			DATE_COLUMN = "practicesession_date",
			DURATION_COLUMN = "duration",
			GUIDED_COLUMN = "guided",
			USER_ID_COLUMN = "user_id";
	
	@DatabaseField(generatedId=true, columnName=ID_COLUMN)
	public int id;
	@DatabaseField(canBeNull=false, columnName=DURATION_COLUMN)
	public double duration;
	@DatabaseField(canBeNull=false, columnName=GUIDED_COLUMN)
	public boolean guided;
	@DatabaseField(canBeNull=false, columnName=DATE_COLUMN)
	private Date date;
	@DatabaseField(foreign=true, foreignAutoRefresh=true, canBeNull=false, columnName=USER_ID_COLUMN)
	private User user;
	
	public PracticeSession() {
//		DatabaseHelper needs a no arg constructor
	}

	public PracticeSession(double duration, boolean guided, Date date, User user) {
		super();
		this.duration = duration;
		this.guided = guided;
		this.date = new Date(date.getTime());
		this.user = user;
	}

	public Date getDate() {
		return new Date(date.getTime());
	}

	public void setDate(Date date) {
		this.date = new Date(date.getTime());
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "PracticeSession [id=" + id + ", duration=" + duration
				+ ", guided=" + guided + ", date=" + date + ", user=" + user
				+ "]";
	}

}
