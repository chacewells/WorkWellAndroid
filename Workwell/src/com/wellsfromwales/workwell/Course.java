package com.wellsfromwales.workwell;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName=Course.TABLE_NAME)
public class Course {
	public static final String TABLE_NAME = "Courses",
			ID_COLUMN = "_id",
			NAME_COLUMN = "name";
	
	@DatabaseField(generatedId=true, columnName=ID_COLUMN)
	public int id;
	@DatabaseField(canBeNull=false, columnName=NAME_COLUMN)
	public String name;
	
	public Course() {
//		DatabaseHelper needs a no arg constructor
	}

	public Course(String name) {
		super();
		this.name = name;
	}

	@Override
	public String toString() {
		return "Course [id=" + id + ", name=" + name + "]";
	}
}
