package com.wellsfromwales.workwell;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName=User.TABLE_NAME)
public class User {
	public static final String TABLE_NAME = "Users",
			ID_COLUMN = "_id",
			FIRST_NAME_COLUMN = "firstname",
			LAST_NAME_COLUMN = "lastname",
			PASSWORD_COLUMN = "password",
			USERNAME_COLUMN = "username",
			COURSE_ID_COLUMN = "course_id",
			LOCATION_ID_COLUMN = "location_id";
	
	@DatabaseField(generatedId=true, columnName=ID_COLUMN)
	public int id;
	@DatabaseField(canBeNull=false, columnName=USERNAME_COLUMN)
	public String username;
	@DatabaseField(canBeNull=false, columnName=PASSWORD_COLUMN)
	public String password;
	@DatabaseField(columnName=FIRST_NAME_COLUMN)
	public String firstName;
	@DatabaseField(columnName=LAST_NAME_COLUMN)
	public String lastName;
	
	@DatabaseField(foreign=true, foreignAutoRefresh=true, columnName=LOCATION_ID_COLUMN)
	private Location location;
	@DatabaseField(foreign=true, foreignAutoRefresh=true, columnName=COURSE_ID_COLUMN)
	private Course course;
	
	public User() {
//		DatabaseHelper needs a no arg constructor
	}
	
	private User(UserBuilder userBuilder) {
		this.username = userBuilder.username;
		this.password = userBuilder.password;
		this.firstName = userBuilder.firstName;
		this.lastName = userBuilder.lastName;
		this.location = userBuilder.location;
		this.course = userBuilder.course;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}
	
	// builder pattern for flexible constructor
	public static class UserBuilder {
		private String username;
		private String password;
		private String firstName;
		private String lastName;
		private Location location;
		private Course course;
		
		public UserBuilder(String username, String password, Location location, Course course) {
			this.username = username;
			this.password = password;
			this.location = location;
			this.course = course;
		}
		
		public UserBuilder firstName(String firstName) {
			this.firstName = firstName;
			return this;
		}
		
		public UserBuilder lastName(String lastName) {
			this.lastName = lastName;
			return this;
		}
		
		public User build() {
			return new User(this);
		}
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password="
				+ password + ", firstName=" + firstName + ", lastName="
				+ lastName + ", location=" + location + ", course=" + course
				+ "]";
	}
	
}
