package com.wellsfromwales.workwell;

import android.os.Bundle;

import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.dao.RuntimeExceptionDao;

public abstract class WorkwellActivity extends OrmLiteBaseActivity<DatabaseHelper>{
	public static final String USER_TAG = "User",
			MMI_TAG = "MindfulMinuteInstance",
			MMT_TAG = "MindfulMinuteTemplate",
			GM_TAG = "GuidedMeditation";
	
	private RuntimeExceptionDao<Course, Integer> courseDao;
	private RuntimeExceptionDao<Location, Integer> locationDao;
	private RuntimeExceptionDao<User, Integer> userDao;
	private RuntimeExceptionDao<MindfulMinuteTemplate, Integer> mmtDao;
	private RuntimeExceptionDao<MindfulMinuteInstance, Integer> mmiDao;
	private RuntimeExceptionDao<GuidedMeditation, Integer> guidedMeditationDao;
	private RuntimeExceptionDao<AudioFile, Integer> audioFileDao;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	public RuntimeExceptionDao<Course, Integer> getCourseDao() {
		if (courseDao == null)
			courseDao = getHelper().getCourseRuntimeExceptionDao();
		return courseDao;
	}

	public RuntimeExceptionDao<Location, Integer> getLocationDao() {
		if (locationDao == null)
			locationDao = getHelper().getLocationRuntimeExceptionDao();
		return locationDao;
	}

	public RuntimeExceptionDao<User, Integer> getUserDao() {
		if (userDao == null)
			userDao = getHelper().getUserRuntimeExceptionDao();
		return userDao;
	}

	public RuntimeExceptionDao<MindfulMinuteTemplate, Integer> getMmtDao() {
		if (mmtDao == null)
			mmtDao = getHelper().getMindfulMinuteTemplateRuntimeExceptionDao();
		return mmtDao;
	}

	public RuntimeExceptionDao<MindfulMinuteInstance, Integer> getMmiDao() {
		if (mmiDao == null)
			mmiDao = getHelper().getMindfulMinuteInstanceRuntimeExceptionDao();
		return mmiDao;
	}

	public RuntimeExceptionDao<GuidedMeditation, Integer> getGuidedMeditationDao() {
		if (guidedMeditationDao == null)
			guidedMeditationDao = getHelper().getGuidedMeditationRuntimeExceptionDao();
		return guidedMeditationDao;
	}

	public RuntimeExceptionDao<AudioFile, Integer> getAudioFileDao() {
		if (audioFileDao == null)
			audioFileDao = getHelper().getAudioFileRuntimeExceptionDao();
		return audioFileDao;
	}
}
