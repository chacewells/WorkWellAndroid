package com.wellsfromwales.workwell;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName=AudioFile.TABLE_NAME)
public class AudioFile {
	public static final String TABLE_NAME = "AudioFiles",
			ID_COLUMN = "_id",
			NAME_COLUMN = "name",
			RESOURCE_ID_COLUMN = "resource_id";
	
	@DatabaseField(generatedId=true, columnName=ID_COLUMN)
	public int id;
	@DatabaseField(canBeNull=false, columnName=NAME_COLUMN)
	public String name;
	@DatabaseField(canBeNull=false, columnName=RESOURCE_ID_COLUMN)
	public int resourceId;
	
	public AudioFile() {
//		DatabaseHelper needs a no arg constructor
	}

	public AudioFile(String name, int resourceId) {
		super();
		this.name = name;
		this.resourceId = resourceId;
	}
	
	public AudioFile(String name, String basename) {
		super();
		this.name = name;
		this.resourceId = getResourceIdFromBasename(basename);
	}
	
	/**
	 * Returns resource basename without extension or absolute path (i.e. resource 'R.raw.music' returns "music")
	 */
	public String getBasename() {
		String slashSplitter = "/(?=[^/]+$)",
				dotSplitter = "\\.(?=[^\\.]+$)",
				slashMatcher = "(.*/.+)+",
				dotMatcher = ".+\\..+";
		String resourceString = WorkwellApplication.getContext().getResources().getString(this.resourceId);
		
		if (resourceString.matches(slashMatcher))
			resourceString = resourceString.split(slashSplitter)[1];
		if (resourceString.matches(dotMatcher))
			resourceString = resourceString.split(dotSplitter)[0];
		
		return resourceString;
	}
	
	public String getAbsolutePath() {
		return WorkwellApplication.getContext().getResources().getString(this.resourceId);
	}
	
	private static int getResourceIdFromBasename(String basename) {
		return WorkwellApplication.getContext().getResources().getIdentifier(basename, "raw", "com.wellsfromwales.workwell");
	}

	@Override
	public String toString() {
		return "AudioFile [id=" + id + ", name=" + name + ", resourceId="
				+ getBasename() + "]";
	}

}
