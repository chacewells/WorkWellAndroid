package com.wellsfromwales.workwell.activity;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;

import com.wellsfromwales.workwell.R;

public class WorkwellMainActivity extends FragmentActivity {
	
	private static final String[] TAB_TITLES = {"Mindful Minute", "Guided Meditation", "Meditation Timer", "User Info"};
	private static final int NUM_PAGES = TAB_TITLES.length;
	
	private ViewPager mPager;
	private PagerAdapter mPagerAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.workwell_top_pager);
		
		mPager = (ViewPager)findViewById(R.id.pager);
		mPagerAdapter = new WorkwellTopPagerAdapter(getSupportFragmentManager());
		mPager.setAdapter(mPagerAdapter);
		
		final ActionBar actionBar = getActionBar();
		
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		ActionBar.TabListener tabListener = new ActionBar.TabListener() {

			@Override
			public void onTabReselected(Tab arg0,
					android.app.FragmentTransaction arg1) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTabSelected(Tab tab,
					android.app.FragmentTransaction fragmentTransaction) {
				mPager.setCurrentItem(tab.getPosition());
			}

			@Override
			public void onTabUnselected(Tab arg0,
					android.app.FragmentTransaction arg1) {
				// TODO Auto-generated method stub
				
			}
		};
		
		for (String tabTitle : TAB_TITLES) {
			actionBar.addTab(actionBar.newTab().setText(tabTitle).setTabListener(tabListener));
		}
		
		mPager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int position) {
				getActionBar().setSelectedNavigationItem(position);
			}
			
			@Override
			public void onPageScrolled(int position, float positionOffSet, int positionOffsetPixels) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onPageScrollStateChanged(int position) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	private class WorkwellTopPagerAdapter extends FragmentPagerAdapter {
		public GuidedMeditationPickerFragment guidedMeditationPickerFragment = null;
		public MeditationTimerFragment meditationTimerFragment = null;
		public MindfulMinutePickerFragment mindfulMinutePickerFragment = null;
		public UserInfoFragment userInfoFragment = null;
		
		public WorkwellTopPagerAdapter(FragmentManager fragmentManager) {
			super(fragmentManager);
		}

		@Override
		public Fragment getItem(int position) {
			switch (position) {
				case 0:
					return getMindfulMinutePickerFragment();
				case 1:
					return getGuidedMeditationPickerFragment();
				case 2:
					return getMeditationTimerFragment();
				case 3:
					return getUserInfoFragment();
				default:
					return null;
			}
		}

		@Override
		public int getCount() {
			return NUM_PAGES;
		}

		public GuidedMeditationPickerFragment getGuidedMeditationPickerFragment() {
			if (guidedMeditationPickerFragment == null)
				guidedMeditationPickerFragment = new GuidedMeditationPickerFragment();
			return guidedMeditationPickerFragment;
		}

		public MeditationTimerFragment getMeditationTimerFragment() {
			if (meditationTimerFragment == null)
				meditationTimerFragment = new MeditationTimerFragment();
			return meditationTimerFragment;
		}

		public MindfulMinutePickerFragment getMindfulMinutePickerFragment() {
			if (mindfulMinutePickerFragment == null)
				mindfulMinutePickerFragment = new MindfulMinutePickerFragment();
			return mindfulMinutePickerFragment;
		}

		public UserInfoFragment getUserInfoFragment() {
			if (userInfoFragment == null)
				userInfoFragment = new UserInfoFragment();
			return userInfoFragment;
		}
		
	}

}
