package com.wellsfromwales.workwell;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName=Location.TABLE_NAME)
public class Location {
	public static final String TABLE_NAME = "Locations",
			ID_COLUMN = "_id",
			NAME_COLUMN = "name";
	
	@DatabaseField(generatedId=true, columnName=ID_COLUMN)
	public int id;
	@DatabaseField(canBeNull=false, columnName=NAME_COLUMN)
	public String name;
	
	public Location() {
//		DatabaseHelper needs a no arg constructor
	}

	public Location(String name) {
		super();
		this.name = name;
	}

	@Override
	public String toString() {
		return "Location [id=" + id + ", name=" + name + "]";
	}
}
